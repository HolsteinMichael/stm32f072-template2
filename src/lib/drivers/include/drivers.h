/*******************************************************************************
 * @title   drivers.h
 * @author  deexschs (Daniel Schnell)
 * @brief   This functions includes all driver functions
 *******************************************************************************/

#ifndef __DRIVERS_H__
#define __DRIVERS_H__

/* Includes ------------------------------------------------------------------*/



#ifdef __cplusplus
extern "C"
{
#endif

/* Typedefs ------------------------------------------------------------------*/


/* Function declarations -----------------------------------------------------*/

void drivers_clocks_init();



#ifdef __cplusplus
}
#endif

#endif /* __DRIVERS_H__ */

/* EOF */
