/*
 * drivers_i2c.c
 *
 *  Created on: 30.03.2014
 *      Author: dschnell
 */

#include <stdint.h>
#include <stdbool.h>

#include "stm32f0xx_i2c.h"
#include "drivers_i2c.h"

#ifdef I2C_CPAL

#include "stm32f0xx_i2c_cpal.h"

#define I2C_DBG

#ifdef I2C_DBG
#include "dbg_pin.h"
#endif

/* defines */


//#define I2C_TIMING           (0x20D22E37)
//#define I2C_TIMING           (0x00210507)
#define I2C_TIMING           (0X50321312)
//#define I2C_TIMING                  (0x1045061D)



/* global variables */
static CPAL_TransferTypeDef i2c_rx_buf, i2c_tx_buf;
static bool APPTransferComplete = false;


/* forward declarations */
static bool get_APPTransferComplete();
static void set_APPTransferComplete(bool transfer_complete);

/* function definitions */


bool i2c_init()
{
    bool rv = false;

    /* Set all fields to default values */
    (void) CPAL_I2C_StructInit(&I2C2_DevStructure);

    I2C2_DevStructure.CPAL_Dev = CPAL_I2C2;
    I2C2_DevStructure.CPAL_Direction = CPAL_DIRECTION_TXRX;
    I2C2_DevStructure.CPAL_Mode = CPAL_MODE_MASTER;

    //I2C2_DevStructure.CPAL_ProgModel = CPAL_PROGMODEL_INTERRUPT;
    I2C2_DevStructure.CPAL_ProgModel = CPAL_PROGMODEL_DMA;
    I2C2_DevStructure.wCPAL_Options  = CPAL_OPT_DMATX_TCIT | CPAL_OPT_DMARX_TCIT ;

    I2C2_DevStructure.CPAL_State = CPAL_STATE_READY;
    I2C2_DevStructure.pCPAL_I2C_Struct->I2C_Mode = I2C_Mode_I2C;
    I2C2_DevStructure.pCPAL_I2C_Struct->I2C_AnalogFilter =
            I2C_AnalogFilter_Enable;
    I2C2_DevStructure.pCPAL_I2C_Struct->I2C_DigitalFilter = 0x00;
    I2C2_DevStructure.pCPAL_I2C_Struct->I2C_OwnAddress1 = 0x00;
    I2C2_DevStructure.pCPAL_I2C_Struct->I2C_Ack = I2C_Ack_Enable;
    I2C2_DevStructure.pCPAL_I2C_Struct->I2C_AcknowledgedAddress =
            I2C_AcknowledgedAddress_7bit;
    I2C2_DevStructure.pCPAL_I2C_Struct->I2C_Timing = I2C_TIMING;
    I2C2_DevStructure.pCPAL_TransferRx = &i2c_rx_buf;
    I2C2_DevStructure.pCPAL_TransferTx = &i2c_tx_buf;

    /* Initialize CPAL peripheral with the selected parameters */
    if (CPAL_PASS == CPAL_I2C_Init(&I2C2_DevStructure))
    {
        rv = true;
    }
    return rv;
}


bool i2c_dev_ready(uint32_t dev_addr)
{
    I2C2_DevStructure.pCPAL_TransferTx = &i2c_tx_buf;
    I2C2_DevStructure.pCPAL_TransferTx->wAddr1 = dev_addr;

    return (CPAL_PASS == CPAL_I2C_IsDeviceReady(&I2C2_DevStructure));
}


bool i2c_write(uint32_t dev_addr, uint8_t addr, uint8_t val)
{
    bool rv = false;

    /* Disable all options */
    I2C2_DevStructure.wCPAL_Options = 0;
    /* point to CPAL_TransferTypeDef structure */
    I2C2_DevStructure.pCPAL_TransferTx = &i2c_tx_buf;

    /* Configure transfer parameters */
    I2C2_DevStructure.pCPAL_TransferTx->wNumData = sizeof(val);
    I2C2_DevStructure.pCPAL_TransferTx->pbBuffer = &val ;
    I2C2_DevStructure.pCPAL_TransferTx->wAddr1   = dev_addr;
    I2C2_DevStructure.pCPAL_TransferTx->wAddr2   = (uint32_t) addr;
#ifdef I2C_DBG
    dbg_pin(0x1, true);
#endif
    if (CPAL_I2C_Write(&I2C2_DevStructure) == CPAL_PASS )
    {
        while ((I2C2_DevStructure.CPAL_State != CPAL_STATE_READY) && (I2C2_DevStructure.CPAL_State != CPAL_STATE_ERROR) )
            { }

        if (I2C2_DevStructure.CPAL_State != CPAL_STATE_ERROR)
            rv = true;
    }
    else
    {
        /* I2C bus or peripheral is not able to start communication: Error management */
    }
#ifdef I2C_DBG
    dbg_pin(0x1, false);
#endif
    return rv;
}


bool i2c_read(uint32_t dev_addr, uint8_t addr, uint8_t* val)
{
    /* Disable all options */
    I2C2_DevStructure.wCPAL_Options = 0;
    /* point to CPAL_TransferTypeDef structure */
    I2C2_DevStructure.pCPAL_TransferTx = &i2c_rx_buf;

    /* Configure transfer parameters */
    I2C2_DevStructure.pCPAL_TransferTx->wNumData = sizeof(*val);
    I2C2_DevStructure.pCPAL_TransferTx->pbBuffer = val ;
    I2C2_DevStructure.pCPAL_TransferTx->wAddr1   = dev_addr;
    I2C2_DevStructure.pCPAL_TransferTx->wAddr2   = (uint32_t) addr;
    int tries = 10;
#ifdef I2C_DBG
    dbg_pin(0x1, true);
#endif
    bool rv = false;
    if (tries > 0)
    {
        if (CPAL_I2C_Read(&I2C2_DevStructure) == CPAL_PASS )
        {
            while ((I2C2_DevStructure.CPAL_State != CPAL_STATE_READY) && (I2C2_DevStructure.CPAL_State != CPAL_STATE_ERROR) )
                { }

            if (I2C2_DevStructure.CPAL_State != CPAL_STATE_ERROR)
                rv = true;
        }
        else
        {
            /* I2C bus or peripheral is not able to start communication: Error management */
        }
    }
#ifdef I2C_DBG
    dbg_pin(0x1, false);
#endif
    return rv;
}


/**
 * @brief User callback that manages the Timeout error.
 * @param pDevInitStruct .
 * @retval None.
 */
uint32_t CPAL_TIMEOUT_UserCallback(CPAL_InitTypeDef* pDevInitStruct)
{
    while(true);
    return CPAL_PASS ; /* This statement will not be reached */
}

/**
 * @brief User callback that manages the I2C peripheral errors.
 * @note Make sure that the define USE_SINGLE_ERROR_CALLBACK is uncommented in
 * the stm32xxxx_i2c_cpal_conf.h file, otherwise this callback will not be functional.
 * @param pDevInitStruct.
 * @param DeviceError. CPAL implementation example (step by step) UM1566
 * 52/66 Doc ID 023577 Rev 1
 * @retval None
 */
void CPAL_I2C_ERR_UserCallback(CPAL_DevTypeDef pDevInstance, uint32_t DeviceError)
{
    while(true);
}

/**
 * @brief Manages the End of Rx transfer event.
 * @param pDevInitStruct .
 * @retval None.
 */
void CPAL_I2C_RXTC_UserCallback (CPAL_InitTypeDef* pDevInitStruct)
{
    /* assuming that APPTransferComplete is global variable
         used by the application */
#ifdef I2C_DBG
    dbg_pin(0x2, true);
#endif
    set_APPTransferComplete(true);
    /* User application function that informs user of the end of
     an operation by toggling LEDs */
    //APP_ToggleLED();
#ifdef I2C_DBG
    dbg_pin(0x2, false);
#endif
}

/**
 * @brief Manages the End of Tx transfer event.
 * @param pDevInitStruct .
 * @retval None.
 */
void CPAL_I2C_TXTC_UserCallback (CPAL_InitTypeDef* pDevInitStruct)
{
    /* assuming that APPTransferComplete is global variable
     used by application */
#ifdef I2C_DBG
    dbg_pin(0x2, true);
#endif

    set_APPTransferComplete(true);
    /* User application function that informs user of the end of
    an operation by toggling LEDs */
    // APP_ToggleLED();
#ifdef I2C_DBG
    dbg_pin(0x2, false);
#endif

    return;
}

/**
 * @brief Manages Tx transfer event.
 * @param pDevInitStruct .
 * @retval None.
 */
void CPAL_I2C_TX_UserCallback (CPAL_InitTypeDef* pDevInitStruct)
{
#ifdef I2C_DBG
    dbg_pin(0x2, true);
#endif

    if(pDevInitStruct->CPAL_State == CPAL_STATE_READY)
    {
        set_APPTransferComplete(true);
    }
#ifdef I2C_DBG
    dbg_pin(0x2, false);
#endif

}

/**
 * @brief Manages Rx transfer event.
 * @param pDevInitStruct .
 * @retval None.
 */
void CPAL_I2C_RX_UserCallback (CPAL_InitTypeDef* pDevInitStruct)
{
#ifdef I2C_DBG
    dbg_pin(0x2, true);
#endif

    if(pDevInitStruct->CPAL_State == CPAL_STATE_READY)
    {
        set_APPTransferComplete(true);
    }
#ifdef I2C_DBG
    dbg_pin(0x2, false);
#endif

}

bool get_APPTransferComplete()
{
    return APPTransferComplete;
}

void set_APPTransferComplete(bool transfer_complete)
{
    APPTransferComplete = transfer_complete;
}
#else

__IO uint32_t  I2C_TIMEOUT = I2C_LONG_TIMEOUT;

/**
  * @brief  Initializes the I2C source clock and IOs used to drive the LM75
  * @param  None
  * @retval None
  */
void i2c_lowlevel_init(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;

  /* DRV_I2C Periph clock enable */
  RCC_APB1PeriphClockCmd(DRV_I2C_CLK, ENABLE);

  /* Configure the I2C clock source. The clock is derived from the HSI */
  RCC_I2CCLKConfig(RCC_I2C1CLK_HSI);

  /* DRV_I2C_SCL_GPIO_CLK, DRV_I2C_SDA_GPIO_CLK
       and DRV_I2C_SMBUSALERT_GPIO_CLK Periph clock enable */
  RCC_AHBPeriphClockCmd(DRV_I2C_SCL_GPIO_CLK | DRV_I2C_SDA_GPIO_CLK, ENABLE);

  /* Connect PXx to I2C_SCL */
  GPIO_PinAFConfig(DRV_I2C_SCL_GPIO_PORT, DRV_I2C_SCL_SOURCE, DRV_I2C_SCL_AF);

  /* Connect PXx to I2C_SDA */
  GPIO_PinAFConfig(DRV_I2C_SDA_GPIO_PORT, DRV_I2C_SDA_SOURCE, DRV_I2C_SDA_AF);

  /* Configure DRV_I2C pins: SCL */
  GPIO_InitStructure.GPIO_Pin = DRV_I2C_SCL_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_Init(DRV_I2C_SCL_GPIO_PORT, &GPIO_InitStructure);

  /* Configure DRV_I2C pins: SDA */
  GPIO_InitStructure.GPIO_Pin = DRV_I2C_SDA_PIN;
  GPIO_Init(DRV_I2C_SDA_GPIO_PORT, &GPIO_InitStructure);
}



/**
  * @brief  Initializes the DRV_I2C.
  * @param  None
  * @retval None
  */
bool i2c_init(void)
{
    I2C_InitTypeDef I2C_InitStructure;

    i2c_lowlevel_init();

    /* DRV_I2C configuration */
    I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
    I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
    I2C_InitStructure.I2C_DigitalFilter = 0x00;
    I2C_InitStructure.I2C_OwnAddress1 = 0x00;
    I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
    I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
    I2C_InitStructure.I2C_Timing = I2C_TIMING;

    /* Apply DRV_I2C configuration after enabling it */
    I2C_Init(DRV_I2C, &I2C_InitStructure);

    /* DRV_I2C Peripheral Enable */
    I2C_Cmd(DRV_I2C, ENABLE);

    return true;
}


/**
  * @brief  Checks the LM75 status.
  * @param  None
  * @retval ErrorStatus: LM75 Status (ERROR or SUCCESS).
  */
bool i2c_dev_ready(uint32_t dev_addr)
{
/* DS hackedyhack */
return true;
    uint32_t I2C_TimeOut = I2C_TIMEOUT;

    /* Configure slave address, nbytes, reload, end mode and start or stop generation */
    I2C_TransferHandling(DRV_I2C, dev_addr, 0, I2C_AutoEnd_Mode,
            I2C_No_StartStop);

    /* Clear NACKF and STOPF */
    I2C_ClearFlag(DRV_I2C, I2C_ICR_NACKCF | I2C_ICR_STOPCF);

    /* Generate start */
    I2C_GenerateSTART(DRV_I2C, ENABLE);

    /* Wait until timeout elapsed */
    while ((I2C_GetFlagStatus(DRV_I2C, I2C_ISR_STOPF) == RESET)
            && (I2C_TimeOut-- != 0))
        ;

    /* Check if Temp sensor is ready for use */
    if ((I2C_GetFlagStatus(DRV_I2C, I2C_ISR_NACKF) != RESET)
            || (I2C_TimeOut == 0))
    {
        /* Clear NACKF and STOPF */
        I2C_ClearFlag(DRV_I2C, I2C_ICR_NACKCF | I2C_ICR_STOPCF);

        return false;
    }
    else
    {
        /* Clear STOPF */
        I2C_ClearFlag(DRV_I2C, I2C_ICR_STOPCF);

        return true;
    }
}


/**
 * Read from i2c.
 */
bool i2c_read(uint32_t dev_addr, uint8_t addr, uint8_t* val, uint8_t count)
{
    /* Test on BUSY Flag */
    I2C_TIMEOUT = I2C_LONG_TIMEOUT;
    while (I2C_GetFlagStatus(DRV_I2C, I2C_ISR_BUSY) != RESET)
    {
        if ((I2C_TIMEOUT--) == 0)
            return false;
    }

    /* Configure slave address, nbytes, reload, end mode and start or stop generation */
    I2C_TransferHandling(DRV_I2C, dev_addr, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);

    /* Wait until TXIS flag is set */
    I2C_TIMEOUT = I2C_LONG_TIMEOUT;
    while (I2C_GetFlagStatus(DRV_I2C, I2C_ISR_TXIS) == RESET)
    {
        if ((I2C_TIMEOUT--) == 0)
            return false;
    }

    /* Send Register address */
    I2C_SendData(DRV_I2C, (uint8_t) addr);

    /* Wait until TC flag is set */
    I2C_TIMEOUT = I2C_LONG_TIMEOUT;
    while (I2C_GetFlagStatus(DRV_I2C, I2C_ISR_TC) == RESET)
    {
        if ((I2C_TIMEOUT--) == 0)
            return false;
    }

    /* Configure slave address, nbytes, reload, end mode and start or stop generation */
    I2C_TransferHandling(DRV_I2C, dev_addr, count, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);

    /* Wait until RXNE flag is set */
    for (uint8_t i = 0; i<count; ++i)
    {
        I2C_TIMEOUT = I2C_LONG_TIMEOUT;
        while (I2C_GetFlagStatus(DRV_I2C, I2C_ISR_RXNE) == RESET)
        {
            if ((I2C_TIMEOUT--) == 0)
                return false;
        }

        /* Read data from RXDR */
        val[i] = I2C_ReceiveData(DRV_I2C);
    }

    /* Wait until STOPF flag is set */
    I2C_TIMEOUT = I2C_LONG_TIMEOUT;
    while (I2C_GetFlagStatus(DRV_I2C, I2C_ISR_STOPF) == RESET)
    {
        if ((I2C_TIMEOUT--) == 0)
            return false;
    }

    /* Clear STOPF flag */
    I2C_ClearFlag(DRV_I2C, I2C_ICR_STOPCF);

    return true;
}

/**
 * Write to i2c.
 */

bool i2c_write(uint32_t dev_addr, uint8_t addr, const uint8_t* val, uint8_t count)
{
    /* Test on BUSY Flag */
    I2C_TIMEOUT = I2C_LONG_TIMEOUT;
    while (I2C_GetFlagStatus(DRV_I2C, I2C_ISR_BUSY) != RESET)
    {
        if ((I2C_TIMEOUT--) == 0)
            return false;
    }

    /* Configure slave address, nbytes, reload, end mode and start or stop generation */
    I2C_TransferHandling(DRV_I2C, dev_addr, 1, I2C_Reload_Mode,
            I2C_Generate_Start_Write);

    /* Wait until TXIS flag is set */
    I2C_TIMEOUT = I2C_LONG_TIMEOUT;
    while (I2C_GetFlagStatus(DRV_I2C, I2C_ISR_TXIS) == RESET)
    {
        if ((I2C_TIMEOUT--) == 0)
            return false;
    }

    /* Send Register address */
    I2C_SendData(DRV_I2C, addr);

    /* Wait until TCR flag is set */
    I2C_TIMEOUT = I2C_LONG_TIMEOUT;
    while (I2C_GetFlagStatus(DRV_I2C, I2C_ISR_TCR) == RESET)
    {
        if ((I2C_TIMEOUT--) == 0)
            return false;
    }

    /* Configure slave address, nbytes, reload, end mode and start or stop generation */
    I2C_TransferHandling(DRV_I2C, dev_addr, 1, I2C_AutoEnd_Mode,
            I2C_No_StartStop);

    /* Wait until TXIS flag is set */
    for (uint8_t i = 0; i<count; ++i)
        {
        I2C_TIMEOUT = I2C_LONG_TIMEOUT;
        while (I2C_GetFlagStatus(DRV_I2C, I2C_ISR_TXIS) == RESET)
        {
            if ((I2C_TIMEOUT--) == 0)
                return false;
        }

        /* Write data to TXDR */
        I2C_SendData(DRV_I2C, val[i]);
    }

    /* Wait until STOPF flag is set */
    I2C_TIMEOUT = I2C_LONG_TIMEOUT;
    while (I2C_GetFlagStatus(DRV_I2C, I2C_ISR_STOPF) == RESET)
    {
        if ((I2C_TIMEOUT--) == 0)
            return false;
    }

    /* Clear STOPF flag */
    I2C_ClearFlag(DRV_I2C, I2C_ICR_STOPCF);

    return true;
}

#endif
/* EOF */

