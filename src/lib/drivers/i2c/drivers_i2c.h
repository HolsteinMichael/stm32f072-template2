/*
 * drivers_i2c.h
 *
 *  Created on: 30.03.2014
 *      Author: dschnell
 */

#ifndef DRIVERS_I2C_H_
#define DRIVERS_I2C_H_

#include <stdint.h>
#include <stdbool.h>

/* Defines */
#define DRV_I2C                         I2C2
#define DRV_I2C_CLK                     RCC_APB1Periph_I2C2

#define DRV_I2C_SCL_PIN                 GPIO_Pin_10
#define DRV_I2C_SCL_GPIO_PORT           GPIOB
#define DRV_I2C_SCL_GPIO_CLK            RCC_AHBPeriph_GPIOB
#define DRV_I2C_SCL_SOURCE              GPIO_PinSource10
#define DRV_I2C_SCL_AF                  GPIO_AF_1

#define DRV_I2C_SDA_PIN                 GPIO_Pin_11
#define DRV_I2C_SDA_GPIO_PORT           GPIOB
#define DRV_I2C_SDA_GPIO_CLK            RCC_AHBPeriph_GPIOB
#define DRV_I2C_SDA_SOURCE              GPIO_PinSource11
#define DRV_I2C_SDA_AF                  GPIO_AF_1

/* 100 kHz 1000ns rise, 300ns fall, 48MHz i2c clock */
//#define I2C_TIMING     0x30E32E37
/* 400 kHz 300ns rise, 300ns fall, 48MHz i2c clock */
#define I2C_TIMING     0x10950C1C

/* Maximum Timeout values for flags and events waiting loops. These timeouts are
   not based on accurate values, they just guarantee that the application will
   not remain stuck if the I2C communication is corrupted.
   You may modify these timeout values depending on CPU frequency and application
   conditions (interrupts routines ...). */
#define I2C_LONG_TIMEOUT         ((uint32_t)(10 * I2C_FLAG_TIMEOUT))

/* Function declarations */
bool i2c_init();
bool i2c_read(uint32_t dev_addr, uint8_t addr, uint8_t* val, uint8_t count);
bool i2c_write(uint32_t dev_addr, uint8_t addr, const uint8_t* val, uint8_t count);
bool i2c_dev_ready(uint32_t dev_addr);

#endif /* DRIVERS_I2C_H_ */


/* EOF */
